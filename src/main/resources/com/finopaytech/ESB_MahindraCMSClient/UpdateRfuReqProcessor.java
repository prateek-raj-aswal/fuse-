package com.finopaytech.ESB_MahindraCMSClient;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;

public class UpdateRfuReqProcessor {
	
	
	public void UpdateRfu(Exchange exchange){
		
		Message inMessage = exchange.getIn();
		String bodyPara = inMessage.getBody(String.class);
        String token=inMessage.getHeader("XAuthToken",String.class);
        //System.out.println("Token"+token);
        inMessage.setHeader(CxfConstants.OPERATION_NAME, "headerAPI");
        // using the proxy client API
        inMessage.setHeader(CxfConstants.CAMEL_CXF_RS_USING_HTTP_API, Boolean.FALSE);
        inMessage.setHeader(Exchange.ACCEPT_CONTENT_TYPE,"application/vnd.fisglobal-v1.0+json;charset=UTF-8");
		inMessage.setHeader(Exchange.CONTENT_TYPE,"application/vnd.fisglobal-v1.0+json;charset=UTF-8");
        MessageContentsList req = new MessageContentsList();
        req.add(bodyPara);
        req.add(token);
        inMessage.setBody(req);
		
	}

}
